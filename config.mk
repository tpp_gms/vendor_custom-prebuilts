PRODUCT_SOONG_NAMESPACES += \
    vendor/custom-prebuilts/common

# Inherit from telephony config
$(call inherit-product, vendor/custom-prebuilts/products/telephony.mk)

# Inherit from gms config
$(call inherit-product, vendor/custom-prebuilts/products/gms.mk)

# Inherit from audio config
$(call inherit-product, vendor/custom-prebuilts/audio.mk)

# Inherit from rro_overlays config
$(call inherit-product, vendor/custom-prebuilts/rro_overlays.mk)
